package com.jobin.demotestapp

import android.app.Application
import com.jobin.demotestapp.di.AppComponent
import com.jobin.demotestapp.di.DaggerAppComponent
import com.jobin.demotestapp.di.NetworkModule


/**
 * Created by Jobin on 7/4/2019.
 */
class MovieApp:Application(){


var appComponent:AppComponent? = null

    companion object {

        var movieApp:MovieApp?= null

        @JvmStatic
        fun getmovieApp():MovieApp{

            return movieApp!!
        }
    }
    override fun onCreate() {
        super.onCreate()
          movieApp = this
          appComponent = DaggerAppComponent.builder().networkModule(NetworkModule()).build()



    }
    fun getappComponent():AppComponent{

        return appComponent!!
    }

}
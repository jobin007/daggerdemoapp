package com.jobin.demotestapp.presentation.adapter

import android.content.Context
import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.jobin.demotestapp.R
import com.jobin.demotestapp.databinding.MovieBinding
import com.jobin.demotestapp.presentation.viewmodel.MovieViewModel

/**
 * Created by Jobin on 7/3/2019.
 */
class MovieAdapter (context:Context,movies:ArrayList<MovieViewModel>): RecyclerView.Adapter<MovieAdapter.MovieViewHolder>() {

    private var movieList:ArrayList<MovieViewModel> = movies
    private var context:Context                     = context
    private  var inflator:LayoutInflater?           = null
    var movieBinding:MovieBinding?                  = null


    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): MovieViewHolder {

        if (inflator == null){

            inflator = LayoutInflater.from(context)
        }
        movieBinding = DataBindingUtil.inflate(inflator, R.layout.item_view,parent,false)
        return MovieViewHolder((movieBinding as MovieBinding?)!!)

    }

    override fun onBindViewHolder(holder: MovieViewHolder?, position: Int) {

        var movieViewModel:MovieViewModel = movieList[position]
        holder!!.bind(movieViewModel)

    }

    override fun getItemCount(): Int {
       return movieList.size
    }

    class MovieViewHolder (movie: MovieBinding) : RecyclerView.ViewHolder(movie.root) {
      var movie:MovieBinding = movie
      fun bind(movieViewModel:MovieViewModel){

          movie.moviemodel = movieViewModel
          movie.executePendingBindings()

      }
        fun getMovieBinding():MovieBinding{
            return movie
        }
    }
}
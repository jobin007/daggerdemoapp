package com.jobin.demotestapp.presentation.uicomponents

import android.databinding.BindingAdapter
import android.widget.ImageView
import com.squareup.picasso.Picasso

/**
 * Created by Jobin on 7/7/2019.
 */
@BindingAdapter("imageUrl")
fun setImageUrl(view: ImageView,url:String){

    Picasso
            .get()
            .load(url)
            .into(view)

}
package com.jobin.demotestapp.presentation.viewmodel

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.databinding.BindingAdapter
import android.util.Log
import android.widget.ImageView
import com.jobin.demotestapp.MovieApp

import com.jobin.demotestapp.R
import com.jobin.demotestapp.data.model.Data
import com.jobin.demotestapp.data.model.Movie
import com.jobin.demotestapp.data.repository.MovieRepository
import com.jobin.demotestapp.utils.RxHelpers
import com.squareup.picasso.Picasso
import javax.inject.Inject

/**
 * Created by Jobin on 7/3/2019.
 */
class MovieViewModel(): ViewModel() {

    var title: String?  = null
    var year: String?   = null
    var genre: String?  = null
    var poster: String? = null
    var movieMutableLivdata:MutableLiveData<ArrayList<MovieViewModel>> = MutableLiveData()
    var movieList:ArrayList<Data> = ArrayList()

    @Inject
    lateinit var repository:MovieRepository

    init {

        MovieApp.getmovieApp().getappComponent().inject(this)
    }

    fun getImageUrl():String{

        return poster!!
    }





    fun getArrayListMutableData():MutableLiveData<ArrayList<MovieViewModel>>{

        var movies:ArrayList<MovieViewModel> = ArrayList()

        repository.getMovieList()
                .compose(RxHelpers.IOAndMainThreadSchedulers())
                .subscribe({ model ->
                    movieList.addAll(model.data)
                    for(data in movieList){
                        var movieModel = MovieViewModel(data)
                        movies.add(movieModel)
                        movieMutableLivdata!!.value = movies
                    }
                }) { throwable -> Log.e(MovieViewModel::class.java.simpleName, throwable.toString()) }




       return movieMutableLivdata!!

    }

    constructor(data: Data) : this() {

        this.title  = data.title
        this.genre  = data.genre
        this.year   = data.year
        this.poster = data.poster
    }
}



package com.jobin.demotestapp.presentation.view

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import com.jobin.demotestapp.R
import com.jobin.demotestapp.presentation.adapter.MovieAdapter
import com.jobin.demotestapp.presentation.viewmodel.MovieViewModel
import kotlinx.android.synthetic.main.activity_home.*

class MainActivity : BaseActivity() {

   private lateinit var movieViewModel:MovieViewModel
   private lateinit var movieAdapter:MovieAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        movieViewModel = ViewModelProviders.of(this).get(MovieViewModel::class.java)
        movieViewModel.getArrayListMutableData().observe(this, Observer<ArrayList<MovieViewModel>> { items ->

            movieAdapter              = MovieAdapter(this, items!!)
            movies_list.layoutManager = LinearLayoutManager(this)
            movies_list.adapter       = movieAdapter

        })

    }
}

package com.jobin.demotestapp.di

import android.app.Application
import com.jobin.demotestapp.MovieApp
import com.jobin.demotestapp.data.remote.ApiService
import com.jobin.demotestapp.data.repository.MovieRepository
import com.jobin.demotestapp.presentation.viewmodel.MovieViewModel
import dagger.Component
import dagger.Module
import javax.inject.Singleton

/**
 * Created by Jobin on 7/2/2019.
 */
@Singleton
@Component(modules = arrayOf(NetworkModule::class))
interface AppComponent {

    fun inject(repository: MovieRepository)
    fun inject(movieViewModel: MovieViewModel)

}
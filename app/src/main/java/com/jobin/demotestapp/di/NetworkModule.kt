package com.jobin.demotestapp.di

import com.google.gson.FieldNamingPolicy
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.jobin.demotestapp.data.remote.ApiService
import com.jobin.demotestapp.data.repository.MovieRepository
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

/**
 * Created by Jobin on 7/2/2019.
 */
@Module
class NetworkModule {

    @Provides
    @Singleton
    fun provideGSON():Gson = GsonBuilder().create()

    @Provides
    @Singleton
    fun provideOKHttpClient():OkHttpClient{

        var client = OkHttpClient.Builder()
        return client.build()

    }

    @Provides
    @Singleton
    fun provideRetrofit(gson:Gson, client:OkHttpClient):Retrofit{

        return Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl("https://raw.githubusercontent.com/filippella/Sample-API-Files/master/json/")
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .client(client)
                .build()



    }

    @Singleton
    @Provides
    fun provideApi(retrofit: Retrofit): ApiService {
        return retrofit.create(ApiService::class.java)
    }

    @Singleton
    @Provides
    fun provideRepository(service: ApiService):MovieRepository{
        return MovieRepository(service)
    }
}

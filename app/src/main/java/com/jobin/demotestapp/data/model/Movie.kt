package com.jobin.demotestapp.data.model


import com.google.gson.annotations.SerializedName

data class Movie(
    val `data`: List<Data>
)
package com.jobin.demotestapp.data.repository

import android.util.Log
import com.jobin.demotestapp.data.model.Movie
import com.jobin.demotestapp.data.remote.ApiService
import rx.Observable
import java.util.*

import javax.inject.Inject

/**
 * Created by Jobin on 7/4/2019.
 */
class MovieRepository  {

    lateinit var apiService:ApiService

    @Inject
    constructor(service: ApiService){

        this.apiService = service

    }
    fun getMovieList():Observable<Movie>{

        return apiService.getMovieData()
    }
}
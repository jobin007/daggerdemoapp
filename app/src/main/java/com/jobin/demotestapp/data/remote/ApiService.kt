package com.jobin.demotestapp.data.remote


import com.jobin.demotestapp.data.model.Movie
import retrofit2.http.GET
import rx.Observable

/**
 * Created by Jobin on 7/2/2019.
 */
interface ApiService {

    @GET("movies-api.json")
    fun getMovieData(): Observable<Movie>


}
package com.jobin.demotestapp.data.model


import com.google.gson.annotations.SerializedName

data class Data(
        val id: Int,
        var title: String,
        val year: String,
        val genre: String,
        val poster: String
)